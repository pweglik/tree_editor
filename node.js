class Node
{
    
    constructor(value)
    {
        this.value = value;
        this.width = 0;
        this.children = [];
        this.g = null;
        this.text = null;
        this.circle = null;
        this.glow_circle = null;
        this.parent = null;
        this.folded = false;
        this.color = "#dddddd";
        this.marked = false;
        this.dragged = false;
        this.x = 0;
        this.y = 0;
    }

    create_csv(parent)
    {
        let csv_string = '';
        csv_string += parent + this.value + '\n'
        for(let i in this.children)
        {
            csv_string += this.children[i].create_csv(parent + this.value + ',');
        }
        return csv_string;

    }

    add_child(child)
    {
        child.parent = this;
        this.children.push(child);
    }

    calculate_width()
    {
        let sum = 0;
        if(this.children.length != 0 && this.folded == false)
        {
            for(let i in this.children)
            {
                sum += this.children[i].calculate_width();    
            }
            this.width = sum;
        }
        else
        {
            this.width = 20;
        }
        return this.width;
        
    }

    draw(layer, x, root_frame)
    {
        let color_palette = ['#C60638','#10AEBA','#E281BF','#FF7545','#0F7ABC','#ED690E','#5652A3'];
        this.color = color_palette[layer];
        if(this.children.length != 0 && this.folded == false)
        {
            this.children.sort(compare);
            let temp_width = 0;
            for(let i in this.children)
            {
                if(i == 0)
                {
                    temp_width += this.children[i].width/2;
                }
                let tempx = x - this.width/2 + temp_width;
                temp_width += this.children[i].width/2;
                if(i < this.children.length-1)
                {
                   temp_width += this.children[parseInt(i)+1].width/2;
                }
                root_frame.append("path").attr("stroke","#666666").attr("opacity", "0.5").attr("style", "stroke-width:1.5px; fill:none;")
                .attr("d","M " + 300*layer + " " + x + " C " + (300*layer + 150) + " " + x + " " + 
                (300*layer + 150) + " " + tempx + " " + 300*(layer+1) + " " + tempx);

                this.children[i].draw(layer + 1, tempx, root_frame);          
            }
        }

        this.g = root_frame.append("g").attr("id", "clickable").attr("transform", "translate(" + 300 * layer + "," + x + ")");
        this.x = 300 * layer;
        this.y = x;
        this.glow_circle = this.g.append("circle").attr("r", "15").attr("fill", "red").attr("opacity", 0);
        this.circle = this.g.append("circle").attr("r", "5").attr("fill", this.color);
        this.text = this.g.append("text").attr("font-size", "10px").text(this.value).attr("font-family","sans-serif").attr("x", "-10").attr("y", "3").attr("text-anchor","end").attr("class", "unselectable"); 
        

        if(this.marked)
        {
            this.circle.attr("fill", "red")
        }

        let g_handle = document.getElementById("clickable");
        this.g.attr("id","");

        g_handle.addEventListener('contextmenu', event => {
                event.preventDefault();
                document.getElementById("right_click_menu").style = "display: flex; position: fixed; z-index: 2; top:"+event.clientY+"px; left:"+event.clientX+"px;";
                target = this;
        });

        g_handle.addEventListener('mousedown', event => {
            if(event.button == 0)
            {     
                if(ctrl_pressed == true)
                {
                    this.marked = !this.marked;
                    if(this.color != "red")
                    {
                        this.color = "red";
                        g_handle.firstChild.style.fill = "red";
                        targets.push(this);
                    }
                    else
                    {
                        if(layer>6)
                        {
                            this.color = "#aaaaaa";
                            g_handle.firstChild.style.fill = "#aaaaaa";
                        }
                        else
                        {
                            this.color = color_palette[layer];
                            g_handle.firstChild.style.fill = color_palette[layer];
                        }
                        
                        targets = targets.filter(obj => {
                            if(obj != this)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        });

                    }
                }
                else
                {
                    mouse_down = true;
                    drag_copy = this;
                }

            }
            
        });
        
        g_handle.addEventListener("mousemove", event => {
            if(drag_deep_copy == null && drag_copy != null)
            {
                drag_deep_copy = new Node(null)
                drag_deep_copy.value = drag_copy.value
                drag_deep_copy.width = drag_copy.width;
                drag_deep_copy.color = drag_copy.color;
                drag_deep_copy.children = [];

                copy_children(drag_deep_copy,  drag_copy);

                target = drag_copy;
                delete_node();
            }
            
            if( drag_copy != null && drag_deep_copy != null && mouse_down == true)
            {
                
                d3.select("#circle_drag").remove(); 

                svg.append("circle").attr("transform","translate(" + (event.clientX - document.getElementById('svgcontainer').offsetLeft) + "," + (event.clientY) + ")")
                .attr("r", 5*scale).attr("fill", drag_deep_copy.color).attr("id", "circle_drag");

                look_for_smallest_distance(root_node, event)
                
                if(last_closest_node != closest_node && last_closest_node != null)
                {
                    last_closest_node.glow_circle.attr("opacity", 0);
                    last_closest_node.circle.attr("fill", last_closest_node.color);
                }
                
                closest_node.glow_circle.attr("opacity", 0.2);
                closest_node.circle.attr("fill", "red");
                last_closest_node = closest_node;
                min_distance = 100000000;
                closest_node = null;
                
            }
        });

        g_handle.addEventListener('mouseup', event => {
            if(event.button == 0 && ctrl_pressed == false && drag_deep_copy == null)
            {
                this.folded = !this.folded;
            }  

            if( drag_copy != null && drag_deep_copy != null && mouse_down == true)
            {
                look_for_smallest_distance(root_node, event)
                
                let  temp_node = new Node(null)
                temp_node.value = drag_deep_copy.value
                temp_node.width = drag_deep_copy.width;
                temp_node.color = drag_deep_copy.color;
                temp_node.children = [];
        
                copy_children(temp_node, drag_deep_copy);
        
                closest_node.add_child(temp_node);

                min_distance = 100000000;
                closest_node = null;
            }
            refresh_screen();

            mouse_down = false;
            drag_copy = null;
            drag_deep_copy = null;
            d3.select("#circle_drag").remove();  
        });
    }

}


function compare( a, b ) {
    if ( a.value < b.value ){
      return -1;
    }
    if ( a.value > b.value ){
      return 1;
    }
    return 0;
  }