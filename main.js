    var svg = d3.select("#svgcontainer")
    .append("svg")
    .attr("id", "svg")
    .attr("width", window.innerWidth*4/5)
    .attr("height", window.innerHeight);


    svg.append("rect")
    .attr("id", "rect") 
    .attr("width", "100%")
    .attr("height", "100%")
    .attr("fill", "white");
    

    
    let root_frame = svg.append("g").attr("transform", "translate(100,400) scale(1)");
    let root_node = null;
    let target = null;
    let targets = [];
    let copy_target = null;
    let copy_targets = [];
    let ctrl_pressed = false;
    let drag_copy = null, drag_deep_copy = null;
    let mouse_down = false; 
    let translateX = 100, translateY = 400;



    document.getElementById("RootName").focus();

    //dialog windows
    document.getElementById("CreateRootForm").addEventListener("submit", event => 
    {
        event.preventDefault(); 
        create_root_node();
        return false;
    });
    document.getElementById("RenameNodeForm").addEventListener("submit", event => 
    {
        event.preventDefault(); 
        rename_node();
        return false;
    });
    document.getElementById("DeleteNodeForm").addEventListener("submit", event => 
    {
        event.preventDefault(); 
        delete_node();
        return false;
    });
    document.getElementById("CreateChildNodeForm").addEventListener("submit", event => 
    {
        event.preventDefault(); 
        create_child_node();
        return false;
    });
    

    function create_root_node()
    {
        document.getElementById("CreateRootNode").style = "display: none; opacity: 1; visibility: hidden; top: -400px;"

        let text = document.getElementById("RootName").value; 
        
        root_node = new Node(text);
        refresh_screen()
        
    }

    let fileInput = document.getElementById("csvfile"),

    readFile = function () {
        var reader = new FileReader();
        reader.onload = function () {
            let result = reader.result;
            while(result.indexOf("\n") != -1)
            {
                let temp = result.slice(0,result.indexOf("\n"));

                let temp_table = temp.split(",");
                let focus_node = null;

                for(let i in temp_table)
                {
                    if(i == temp_table.length-1)
                    {
                        if(i==0)
                        {
                            root_node = new Node(temp_table[i])
                        }
                        else
                        {
                            let temp_node = new Node(temp_table[i]);
                            focus_node.add_child(temp_node);
                        }
                    }
                    if(i==0)
                    {
                        focus_node = root_node;
                    }
                    else
                    {
                        focus_node = focus_node.children.find(el => el.value == temp_table[i])
                    }
                }
                
                result = result.slice(result.indexOf("\n")+1);
            }
            refresh_screen();
            close_dialog();

        };
        // start reading the file. When it is done, calls the onload event defined above.
        reader.readAsBinaryString(fileInput.files[0]);
        
    };

    function load_example()
    {

        let result = "CS\n\
        CS,Operating Systems\n\
        CS,Operating Systems,Linux\n\
        CS,Operating Systems,Linux,Fedora\n\
        CS,Operating Systems,Linux,Ubuntu\n\
        CS,Operating Systems,Mac\n\
        CS,Operating Systems,Windows\n\
        CS,Theory\n\
        CS,programming languages\n\
        CS,programming languages,C\n\
        CS,programming languages,Java\n\
        CS,programming languages,Python"
        console.log(result)

        while(result.indexOf("\n") != -1)
        {
            let temp = result.slice(0,result.indexOf("\n"));

            let temp_table = temp.split(",");
            let focus_node = null;

            for(let i in temp_table)
            {
                if(i == temp_table.length-1)
                {
                    if(i==0)
                    {
                        root_node = new Node(temp_table[i])
                    }
                    else
                    {
                        let temp_node = new Node(temp_table[i]);
                        focus_node.add_child(temp_node);
                    }
                }
                if(i==0)
                {
                    focus_node = root_node;
                }
                else
                {
                    focus_node = focus_node.children.find(el => el.value == temp_table[i])
                }
            }
            
            result = result.slice(result.indexOf("\n")+1);
        }
        document.getElementById("CreateRootNode").style = "display: none; opacity: 1; visibility: hidden; top: -400px;"
        refresh_screen();
    }

    fileInput.addEventListener('change', readFile);

    function rename_node()
    {
        document.getElementById("RenameNode").style = "display: none; opacity: 1; visibility: hidden; fz-index: 1;";
        let temp_text = document.getElementById("RenameName").value;
        target.value = temp_text;

        target.text.text(temp_text);
        refresh_screen();
        hide_right_click_menu();
    }

    function close_dialog()
    {
        document.getElementById("CreateRootNode").style = "display: none; opacity: 1; visibility: hidden; z-index: 1;";
        document.getElementById("RenameNode").style = "display: none; opacity: 1; visibility: hidden; z-index: 1;";
        document.getElementById("DeleteNode").style = "display: none; opacity: 1; visibility: hidden; z-index: 1;";
        document.getElementById("CreateChildNode").style = "display: none; opacity: 1; visibility: hidden; z-index: 1;";
        hide_right_click_menu()
    }

    function delete_node()
    {
        document.getElementById("DeleteNode").style = "display: none; opacity: 1; visibility: hidden; z-index: 1;";
        
        if(targets.length == 0)
        {
            for(let i in target.parent.children)
            {
                if(target.parent.children[i] == target)
                {
                    target.parent.children.splice(i, 1);
                }
            }
            delete target;
        }
        else
        {
            for(let i in targets)
            {
                target = targets[i];
                for(let i in target.parent.children)
                {
                    if(target.parent.children[i] == target)
                    {
                        target.parent.children.splice(i, 1);
                    }
                }
                delete target;
            }

        }
        targets = [];
        refresh_screen();
        hide_right_click_menu()
    }

    function create_child_node()
    {
        document.getElementById("CreateChildNode").style = "display: none; opacity: 1; visibility: hidden; z-index: 1;";

        let temp_text = document.getElementById("ChildName").value;
        let temp_names = temp_text.split('^');
        for(let i in temp_names)
        {
            let new_child = new Node(temp_names[i]);

            target.add_child(new_child)
        }
        
        refresh_screen();
        hide_right_click_menu()
    }

    


    function refresh_screen()
    {
        //d3.select("#svg").attr("width", window.innerWidth*4/5).attr("height", window.innerHeight);

        d3.select("g").selectAll("g").remove();
        d3.select("g").selectAll("path").remove();
        root_node.calculate_width();
        root_node.draw(0, 0, root_frame);
    }

    //events of main_frame
    let svg_element = document.getElementById("svg");
    let rect = document.getElementById("rect");
    svg_element.addEventListener('wheel', scale_root_frame);
    let x = 0,y = 0,scale = 1, isDragging = false;

    rect.addEventListener('mousedown', event => {
        hide_right_click_menu()
        if(event.button == 0)
        {
            x = event.clientX;
            y = event.clientY;
            isDragging = true;
        }  
    });

    rect.addEventListener('mouseup', event => {
        isDragging = false;
    });

    rect.addEventListener('mousemove', move_root_frame);

    function scale_root_frame(event)
    {
        event.preventDefault();

        let temp = svg_element.childNodes[1].getAttribute("transform")

        scale = temp.substring(temp.indexOf("scale(")+6,temp.length - 1);
        scale = parseFloat(scale) + event.deltaY * -0.0005;

        temp = temp.substring(0, temp.indexOf("scale(")+6) + scale + ')';

        root_frame.attr("transform", temp);
    }

    function move_root_frame(event)
    {
        if(isDragging)
        {
            dtranslateX = x - event.clientX;
            dtranslateY = y - event.clientY;

            let temp = svg_element.childNodes[1].getAttribute("transform")

            translate = temp.substring(temp.indexOf("slate(")+6,temp.indexOf(")"));

            translateX = parseFloat(translate.substring(0,translate.indexOf(","))) - dtranslateX;
            translateY = parseFloat(translate.substring(translate.indexOf(",")+1)) - dtranslateY;

            x = event.clientX;
            y = event.clientY;

            temp = temp.substring(0, temp.indexOf("slate(")+6) + translateX + "," + translateY + ')' + temp.substring( temp.indexOf(" "));
            root_frame.attr("transform", temp);

        }
        
    }

    //context menu - also in mousedown event and in draw() in node.js after g creation 
    function hide_right_click_menu()
    {
        document.getElementById("right_click_menu").style = "display: none;";
    }

    document.getElementById("rename_button").addEventListener('click', event =>{
        close_dialog()
        document.getElementById("RenameNode").style = "display: block; opacity: 1; visibility: visible; z-index: 1;";
        document.getElementById("RenameName").focus();

        hide_right_click_menu()
    });

    document.getElementById("delete_button").addEventListener('click', event =>{
        close_dialog()
        document.getElementById("DeleteNode").style = "display: block; opacity: 1; visibility: visible; z-index: 1;";
        document.getElementById("delete_button_form").focus();

        hide_right_click_menu()
    });

    document.getElementById("create_child_button").addEventListener('click', event =>{
        close_dialog()
        document.getElementById("CreateChildNode").style = "display: block; opacity: 1; visibility: visible; z-index: 1;";
        document.getElementById("ChildName").focus();
        
        hide_right_click_menu()
    });

    document.getElementById("copy_children_button").addEventListener('click', event =>{
        
        copy_targets = [];
        
        for(let i in target.children)
        {
            target.children[i].marked = false;
            document.getElementById("paste_button").disabled = false;
            copy_targets.push( new Node(null) );
            copy_targets[i].value = target.children[i].value
            copy_targets[i].width = target.children[i].width;
            copy_targets[i].children = [];  

            copy_children(copy_targets[i], target.children[i])           
            
        } 
        targets = [];
        
        hide_right_click_menu();
    });

    document.getElementById("copy_button").addEventListener('click', event =>{

        copy_targets = [];
        if(targets.length == 0)
        {
            document.getElementById("paste_button").disabled = false;
            copy_target = new Node(null)
            copy_target.value = target.value
            copy_target.width = target.width;
            copy_target.children = [];

            copy_children(copy_target, target)
        }
        else
        {
            for(let i in targets)
            {
                targets[i].marked = false;
                document.getElementById("paste_button").disabled = false;
                copy_targets.push( new Node(null) );
                copy_targets[i].value = targets[i].value
                copy_targets[i].width = targets[i].width;
                copy_targets[i].children = [];  
    
                copy_children(copy_targets[i], targets[i])           
                
            } 
            targets = [];
        }
        hide_right_click_menu()
    });

    function copy_children(node_to_insert, node_from_scrap)
    {
    
        for(let i in node_from_scrap.children)
        {
            let child = new Node(node_from_scrap.children[i].value);
            child.width = node_from_scrap.children[i].width;
            child.children = [];
            copy_children(child, node_from_scrap.children[i])
            node_to_insert.add_child(child)
        }
        
    }

    document.getElementById("cut_button").addEventListener('click', event =>{

        copy_targets = [];
        if(targets.length == 0)
        {
            document.getElementById("paste_button").disabled = false;
            copy_target = new Node(null)
            copy_target.value = target.value
            copy_target.width = target.width;
            copy_target.children = [];

            copy_children(copy_target, target)
            delete_node()
        }
        else
        {
            for(let i in targets)
            {
                document.getElementById("paste_button").disabled = false;
                copy_targets.push( new Node(null) );
                copy_targets[i].value = targets[i].value;
                copy_targets[i].width = targets[i].width;
                copy_targets[i].children = [];
    
                copy_children(copy_targets[i], targets[i])
            }
            for(let i in targets)
            {
                delete_node()
            }
            //targets = [];
        }
        hide_right_click_menu()
        
    });

    document.getElementById("paste_button").addEventListener('click', event =>{
        if(copy_targets.length == 0)
        {
            let temp = new Node(null)
            temp.value = copy_target.value
            temp.width = copy_target.width;
            temp.children = [];

            copy_children(temp, copy_target)

            if(copy_target != null)
            {
                target.add_child(temp);
                hide_right_click_menu()
                refresh_screen();
            }
        }
        else
        {
            for(let i in copy_targets)
            {
                let temp = new Node(null)
                temp.value = copy_targets[i].value
                temp.width = copy_targets[i].width;
                temp.children = [];

                copy_children(temp, copy_targets[i])

                if(copy_targets[i] != null)
                {
                    target.add_child(temp);
                    hide_right_click_menu()
                    refresh_screen();
                }
            }
        }
        
    });

    //saving to file

    function save_to_file()
    {
        var csv = '';
        csv = root_node.create_csv('')
    
        console.log(csv);
        var hiddenElement = document.getElementById('adownload');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_blank';
        hiddenElement.download = "file.txt"
        hiddenElement.click();
    }

    //random
    document.addEventListener("keydown", function(event) {
        if (event.keyCode == 17) {
            ctrl_pressed = true;
        }
        
    });
    document.addEventListener("keyup", function(event) {
        if (event.keyCode == 17) {
            ctrl_pressed = false;
        }
        
    });

// dragging

let min_distance = 100000000;
let closest_node = null, last_closest_node = null;
let counter = 0;

document.addEventListener("mousemove", event => {
    
    if(drag_deep_copy == null && drag_copy != null)
    {
        drag_deep_copy = new Node(null)
        drag_deep_copy.value = drag_copy.value
        drag_deep_copy.width = drag_copy.width;
        drag_deep_copy.color = drag_copy.color;
        drag_deep_copy.children = [];

        copy_children(drag_deep_copy,  drag_copy);

        target = drag_copy;
        delete_node();
    }
    
    if( drag_copy != null && drag_deep_copy != null && mouse_down == true)
    {
        
        d3.select("#circle_drag").remove(); 
        svg.append("circle").attr("transform","translate(" + (event.clientX - document.getElementById('svgcontainer').offsetLeft ) + "," + (event.clientY) + ")")
        .attr("r", 5*scale).attr("fill", drag_deep_copy.color).attr("id", "circle_drag");

        look_for_smallest_distance(root_node, event)
        
        if(last_closest_node != closest_node && last_closest_node != null)
        {
            last_closest_node.glow_circle.attr("opacity", 0);
            last_closest_node.circle.attr("fill", last_closest_node.color);
        }
        
        closest_node.glow_circle.attr("opacity", 0.2);
        closest_node.circle.attr("fill", "red");
        last_closest_node = closest_node;
        min_distance = 100000000;
        closest_node = null;
        
    }
    
    
});

document.addEventListener('mouseup', event => {
    
    if( drag_copy != null && drag_deep_copy != null && mouse_down == true)
    {   
        look_for_smallest_distance(root_node, event)
        

        let  temp_node = new Node(null)
        temp_node.value = drag_deep_copy.value
        temp_node.width = drag_deep_copy.width;
        temp_node.color = drag_deep_copy.color;
        temp_node.children = [];

        copy_children(temp_node, drag_deep_copy);

        closest_node.add_child(temp_node);
        refresh_screen();
        min_distance = 100000000;
        closest_node = null;
    }

    mouse_down = false;
    drag_copy = null;
    drag_deep_copy = null;
    d3.select("#circle_drag").remove();     
});

function distance(x1,y1,x2,y2)
{
    return Math.sqrt( Math.pow(x1-x2,2) + Math.pow(y1-y2,2) );
}

function look_for_smallest_distance(node, event)
{   
    let node_distance = distance( event.clientX - document.getElementById('svgcontainer').offsetLeft, event.clientY, (node.x * scale + translateX), (node.y *scale+ translateY) );
    if( node_distance < min_distance)
    {
        min_distance = node_distance;
        closest_node = node;
    }

    for(let i in node.children)
    {
        look_for_smallest_distance(node.children[i], event)
    }
}

window.addEventListener('resize', event =>{
    d3.select("#svg")
    .attr("width", window.innerWidth*4/5)
    .attr("height", window.innerHeight);
});





