<!DOCTYPE HTML>
  <html lang="pl">
 	 <head>
		<meta charset="utf-8">
		<title>Tree editor</title>
		
		<meta name="description" content="OPIS"/>
		<meta name="keywors" content="programming" />
		<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge,chrome=1"/>

		<link rel="stylesheet" href="style.css" type="text/css" />
		

 	</head>
  
	<body>
		<div id = "dialogcontainer">

			<div id="CreateRootNode"  class = 'dialogwindow' style="display: block; opacity: 1; visibility: visible; z-index: 1;" role = "dialog">
				<h2 id="modalTitle">Create Root Node</h2>
				<form id = "CreateRootForm">				  
					<textarea id="RootName" placeholder="Type root name to create new tree"></textarea>
					
					<button type ="submit" class="button_accept" >Create</button>
					<button type ="button" class="button_accept" onclick="load_example()">Create an example tree</button>
					<input id="csvfile" type="file">
					
				</form>
				
				
			</div>
			<div id="RenameNode" class = 'dialogwindow' style="display: none; opacity: 1; visibility: hidden; top: -400px; 
			position: fixed; z-index: 1;" role = "dialog">
				<h2 id="modalTitle">Rename Node</h2>
				<form id = "RenameNodeForm">
					<textarea id="RenameName" placeholder="Type new name for the node"></textarea>	
					
					<div>
					  <button type ="button" class="button_cancel" onclick="close_dialog()">Cancel</button>
					  <button type ="submit" class="button_accept">Rename</button>
					</div>
				</form>
			</div>
			<div id="DeleteNode" class = 'dialogwindow' style="display: none; opacity: 1; visibility: hidden; top: -400px; 
			position: fixed; z-index: 1;" role = "dialog">
				<h2 id="modalTitle">Delete Node</h2>
				<form id = "DeleteNodeForm">	
					<div>
					  <button type ="button" class="button_cancel" onclick="close_dialog()">Cancel</button>
					  <button id = "delete_button_form" type ="submit"  class="button_accept">Delete</button>
					</div>
				</form>
			</div>
			<div id="CreateChildNode" class = 'dialogwindow'style="display: none; opacity: 1; visibility: hidden; top: -400px; 
			position: fixed; z-index: 1;" role = "dialog">
				<h2 id="modalTitle">Create Child Node</h2>
				
				<form id = "CreateChildNodeForm">	
					<textarea id="ChildName" placeholder="Type name of a new child. Use '^' to seperate children names in case of adding multiple children"></textarea>	
					
					<div>
						<button type ="button" class="button_cancel" onclick="close_dialog()">Cancel</button>
						<button type ="submit" class="button_accept">Create</button>
					</div>
				</form>
			</div>
			<div class = 'dialogwindow' role = "dialog">
					<h3>Instructions:</h3>
					<ul>
					<li>LMB - show/hide a node and its children</li>
					<li>RMB - more options (create children/rename/copy etc..)</li> 
					<li>drag and drop to move nodes</li> 
					</ul>
			</div>
		

			<div id="SaveToFile" class = 'dialogwindow' style="display: block; opacity: 1; z-index: 1;margin-top: 300px;" role = "dialog">
				<h2 id="modalTitle" style = "margin:0;">Save to File</h2>
					<button class="save_to_files" onclick="save_to_file()">Save</button>
			</div>

		</div>
		<div id = "svgcontainer"></div>
		<div id = "right_click_menu" style="display: none;" class="context_menu">
			<div id = "rename_button">Rename Node</div>
			<div id = "delete_button">Delete Node</div>
			<div id = "create_child_button">Create Children Nodes</div>
			<div id = "copy_children_button">Copy Children Nodes</div>
			<div id = "copy_button">Copy</div>
			<div id = "cut_button">Cut</div>
			<div id = "paste_button" disabled>Paste</div>
		</div>
		<a id = "adownload" style = "visibility:hidden;" download></a>
        <script src="https://d3js.org/d3.v5.min.js"></script>
        <script src="node.js"></script>
        <script src="main.js"></script>
        
	</body>
  
</html>
